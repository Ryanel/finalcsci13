
//===========================================================
// StickyMain.java
// Purpose: Starts the application
// Author: Corwin McKnight
// Updated: May 20, 2019
//===========================================================
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StickyMain extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("./StickyView.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("CSCI 13 Final Project - Global Sticky Note - Corwin McKnight");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}