
//===========================================================
// StickyController.java
// Purpose: Binds the StickyModel to the StickyView.
// Also takes care of managing some parts of the UI.
// Author: Corwin McKnight
// Updated: May 20, 2019
//===========================================================
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.CheckBox;
import java.util.Date;
import java.text.SimpleDateFormat;

public class StickyController implements Initializable {
    // Model
    StickyModel model;

    @FXML
    public Button btnSave;

    @FXML
    public Button btnLoad;

    @FXML
    public CheckBox checkServer;

    @FXML
    public TextArea textArea;

    @FXML
    public Label lblStatus;

    public StickyController() {
        model = new StickyModel();
    }

    public String getDateString() {
        Date date = new java.util.Date();
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM d, y 'at' h:mm a");
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    @FXML
    public void handleButtonAction(ActionEvent e) {
        String text = ((Button) e.getSource()).getText();

        switch (text) {
        default:
        case "Load":
            String s = model.loadNote(checkServer.isSelected());
            textArea.setText(s);
            if (checkServer.isSelected()) {
                lblStatus.setText("Loaded from server on " + getDateString());
            } else {
                lblStatus.setText("Loaded from filesystem on " + getDateString());
            }
            break;
        case "Save":
            boolean result = model.saveNote(checkServer.isSelected(), textArea.getText());
            if (result == true) {
                lblStatus.setText("Saved on " + getDateString());
            } else {
                lblStatus.setText("Unable to save");
            }
            break;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (model.localNoteExists()) {
            String note = model.loadNote(false);
            textArea.setText(note);
            lblStatus.setText("Loaded from filesystem on " + getDateString());
        } else {
            lblStatus.setText("First run, creating blank note");
        }
    }

}