import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.nio.file.*;
import java.io.File;
import java.io.*;

public class StickyModelTest {

    final String TEST_NOTE_A = "This is the contents of Test Note A, if this appears, this means that saving/loading is working";
    final String TEST_NOTE_B = "This is the contents of Test Note B, if this appears, this means that saving/loading is working";
    final String TEST_NOTE_SERVER = "This is the contents of the Server test note.\r\n It contains special characters, like !@#$%^&*()1234567890-=?/.,/../., to ensure that URI escaping works";

    @Test
    public void testSaveStickyLocal() throws IOException {
        StickyModel m = new StickyModel();
        String testString = TEST_NOTE_A;
        // Delete current note
        m.deleteLocalNote();
        // Save note
        assertEquals(true, m.saveNote(false, testString));

        // Verify file was created with correct contents
        String noteContent = new String(Files.readAllBytes(m.getLocalNotePath()));
        assertEquals(testString, noteContent);
    }

    @Test
    public void testLoadStickyLocal() throws IOException {
        StickyModel m = new StickyModel();
        String testString = TEST_NOTE_B;
        // Delete current note
        m.deleteLocalNote();

        // Create test note
        File f = m.getLocalNotePath().toFile();
        f.createNewFile();
        FileWriter writer = new FileWriter(f);
        writer.write(testString);
        writer.close();

        // Attempt to load using load
        String saved_data = m.loadNote(false);

        // Check if note contents are the same as test note
        assertEquals(saved_data, testString);
    }

    @Test
    public void testLoadStickyLocalNone() {
        StickyModel m = new StickyModel();
        // Delete current note
        m.deleteLocalNote();
        // Attempt to load note using load
        String noteContents = m.loadNote(false);
        // Check if empty note was loaded.
        assertEquals("", noteContents);
    }

    @Test
    public void testServer() {
        StickyModel m = new StickyModel();
        // Save a fake test note to server
        m.saveNote(true, "If this appears, then the server has an issue with rapid writes");
        // Save test note to server
        m.saveNote(true, TEST_NOTE_SERVER);
        // Load note from server
        String noteContents = m.loadNote(true);
        // Verify that test note was saved to server
        assertEquals(TEST_NOTE_SERVER, noteContents);
    }

    @Test
    public void deleteNoteWorks() {
        // Delete current note
        StickyModel m = new StickyModel();
        m.deleteLocalNote();

        // Check if note exists
        assertEquals(false, m.localNoteExists());
    }
}
