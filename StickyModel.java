
//===========================================================
// StickyModel.java
// Purpose: Main Model for my custom sticky note API
// Author: Corwin McKnight
// Updated: May 20, 2019
//===========================================================

import com.google.gson.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.*;
import java.io.File;
import java.nio.file.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javafx.scene.image.Image;

public class StickyModel {
    final String API_KEY = "0xABCDEF";
    final String URL_PATH = "http://cmcknight.ddns.net/api/cs/stickynote/note.php";
    final String LOCAL_FILE_NAME = "localnote.txt";

    public Path getLocalNotePath() {
        return Paths.get("./" + LOCAL_FILE_NAME);
    }

    public void deleteLocalNote() {
        try {
            if (localNoteExists()) {
                System.out.println("[io] Deleting " + getLocalNotePath());
                Files.delete(getLocalNotePath());
            } else {
                System.out.println("[io] " + getLocalNotePath() + " does not exist.");
            }
        } catch (IOException e) {
            System.out.println("Unable to delete note!");
            e.printStackTrace();
        }
    }

    public boolean localNoteExists() {
        return Files.exists(getLocalNotePath());
    }

    public String loadNote(boolean useServer) {

        if (useServer) {
            // Make network request...
            try {
                String urlString = URL_PATH + "?key=" + API_KEY + "&type=read";
                System.out.println("GET " + urlString);
                URL apiRequest = new URL(urlString);

                // Now, perform the request
                InputStream is = apiRequest.openStream(); // Can throw
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                JsonElement jse = new JsonParser().parse(br);
                // Cleanup stream
                is.close();
                br.close();

                // Read response
                long status = jse.getAsJsonObject().get("status").getAsLong();
                if (status == 200) {
                    return jse.getAsJsonObject().get("data").getAsString();
                } else {
                    System.out.println("Status was " + status);
                    return jse.getAsJsonObject().get("msg").getAsString();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return "Server error";
        } else {
            System.out.println("LOAD " + getLocalNotePath().toString());

            if (localNoteExists()) {
                try {
                    String content = new String(Files.readAllBytes(getLocalNotePath()));
                    return content;
                } catch (Exception e) {
                    System.out.println("Unable to load note!");
                    e.printStackTrace();
                    return "ERROR";
                }

            } else {
                System.out.println("No note at " + getLocalNotePath().toString());
                return "";
            }
        }
    }

    public boolean saveNote(boolean useServer, String contents) {
        if (useServer) {
            try {
                String urlString = URL_PATH + "?key=" + API_KEY + "&type=write&contents="
                        + URLEncoder.encode(contents, "UTF-8");
                ;
                System.out.println("SAVE " + urlString);
                URL apiRequest = new URL(urlString);

                // Now, perform the request
                InputStream is = apiRequest.openStream(); // Can throw
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                JsonElement jse = new JsonParser().parse(br);

                // Cleanup stream
                is.close();
                br.close();

                // Read response
                long status = jse.getAsJsonObject().get("status").getAsLong();

                if (status == 200) {
                    return true;
                } else {
                    System.out.println("Status was " + status);
                    return false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                System.out.println("SAVE " + getLocalNotePath().toString());
                File f = getLocalNotePath().toFile();
                f.createNewFile();
                FileWriter writer = new FileWriter(f);

                writer.write(contents);
                writer.close();
                return true;
            } catch (IOException e) {
                System.out.println("Unable to save note!");
                e.printStackTrace();
            }

        }
        return false;
    }
}